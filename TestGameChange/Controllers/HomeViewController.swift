
import UIKit
let K_ISSUE_CELL_IDENTIFIER = "IssueCell"

class HomeViewController: UIViewController {

     //MARK:-  IBOutlets
    
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mIndicator: UIActivityIndicatorView!
    
    //MARK:-  Properties
    var mIssuesList : [Issue] = []
    lazy var mViewModel: IssueViewModel = {
        return IssueViewModel()
    }()
    
    //MARK:-  Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mViewModel.checkAndRemoveLocalData()
        configureUI()
        initViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Open Issues List"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "issue_detail_segue") {
            let lDetailVC = segue.destination as! DetailViewController
            lDetailVC.mViewModel = self.mViewModel
            lDetailVC.mIssueInfo = sender as? Issue
        }
    }
    
    deinit {
       // print("deinit HomeViewController")
    }
    
    //MARK:-  Helper Functions
    
    func configureUI() {
        mTableView.tableFooterView = UIView()
        mTableView.backgroundView = nil
        mTableView.estimatedRowHeight = 44.0
        mTableView.rowHeight = UITableView.automaticDimension
    }
    
    func initViewModel() {
        fetchAllOpenIssues()
        mViewModel.refreshViewClouser = { [weak self] error  in
            self?.mIssuesList.removeAll()
            DispatchQueue.main.async {
                self?.mIndicator.stopAnimating()
                if let lError = error {
                    self?.showAlertView(withMessage: lError, andTitle: "Error")
                }
                if let lIssueList = DatabaseManager.fetchIssueListFromLocalStroage(), !lIssueList.isEmpty {
                    self?.mIssuesList.append(contentsOf: lIssueList)
                }
                self?.reloadTableView()
            }
        }
    }
    
    func reloadTableView()  {
       mTableView.reloadData()
    }
    
    func fetchAllOpenIssues() {
        if let lIssueList = DatabaseManager.fetchIssueListFromLocalStroage(), !lIssueList.isEmpty {
            mIssuesList.append(contentsOf: lIssueList)
            reloadTableView()
        } else {
            mIndicator.startAnimating()
            mViewModel.getIssuesListFromServer()
        }
    }
    
}

//MARK:-  UITableViewDataSource & UITableViewDelegate
extension HomeViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        mIssuesList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: K_ISSUE_CELL_IDENTIFIER, for: indexPath) as? IssueCell
        cell?.mIssueInfo = mIssuesList[indexPath.row]
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "issue_detail_segue", sender: mIssuesList[indexPath.row])
    }
}
