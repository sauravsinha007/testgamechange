
import UIKit

let K_COMMENT_CELL_IDENTIFIER = "CommentCell"

class DetailViewController: UIViewController {
    
    //MARK:-  IBOutlets
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mIndicator: UIActivityIndicatorView!
    
    //MARK:-  Properties
    var mCommentList : [Comment] = []
    var mViewModel: IssueViewModel?
    var mIssueInfo : Issue?
    
     //MARK:-  Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureUI()
        congfigureViewModel()
    }
    
    deinit {
       // print("deinit DetailViewController")
    }
    
    //MARK:-  Helper Functions
   
    func configureUI() {
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationItem.title = "Comment(s) #" + "\(mIssueInfo?.number ?? 0)"
        mTableView.tableFooterView = UIView()
        mTableView.backgroundView = nil
        mTableView.estimatedRowHeight = 44.0
        mTableView.rowHeight = UITableView.automaticDimension
    }
    
    func congfigureViewModel() {
        
        fetchCommentOnIssue()
        
        mViewModel?.refreshDetailViewClouser = { [weak self] error  in
            self?.mCommentList.removeAll()
            DispatchQueue.main.async {
                self?.mIndicator.stopAnimating()
                if let lError = error {
                    self?.showAlertView(withMessage: lError, andTitle: "Error")
                }
                if let lIssueNo = self?.mIssueInfo?.number, let lComments = DatabaseManager.fetchCommenFromDbHaving(issueNo: lIssueNo) {
                    self?.mCommentList.append(contentsOf: lComments)
                }
                self?.refreshCommentView()
            }
        }
    }
    
    func fetchCommentOnIssue() {
        if let lIssueNo = mIssueInfo?.number {
            if let lComments = DatabaseManager.fetchCommenFromDbHaving(issueNo: lIssueNo), !lComments.isEmpty {
                mCommentList.removeAll()
                mCommentList.append(contentsOf: lComments)
                refreshCommentView()
            }
            else {
                mIndicator.startAnimating()
                mViewModel?.getCommentsOnIssuesFromServerHaving(issueNo: Int(lIssueNo))
            }
        }
    }
    
    func refreshCommentView() {
        if mCommentList.count == 0 {
            Utils.showAlert(self,
                            message: K_COMMENT_NOT_AVAILABLE_MSG,
                            title: "Alert",
                            cancelTitle: "OK",
                            cancelAction: { action in
                                if action.title == "OK" {
                                    self.dismissDetailScreen()
                                }
                        })
            return
        }
        mTableView.reloadData()
    }

    func dismissDetailScreen() {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:-  UITableViewDataSource & UITableViewDelegate
extension DetailViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        mCommentList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: K_COMMENT_CELL_IDENTIFIER, for: indexPath) as? CommentCell
        cell?.mCommentInfo = mCommentList[indexPath.row]
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
