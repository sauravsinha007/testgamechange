
import UIKit
let K_MAX_BODY_CHARACTER = 140

class IssueCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bodyLbl: UILabel!
    
    var mIssueInfo : Issue? {
        didSet {
            configureIssueCellUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessoryType = .disclosureIndicator
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func configureIssueCellUI() {
        titleLbl.text = mIssueInfo?.title ?? ""
        //bodyLbl.attributedText = NSAttributedString.init(string: getBodyText())
        bodyLbl.text = getBodyText()
    }
    
    func getBodyText() -> String {
        var lString : String = mIssueInfo?.body ?? ""
        if(lString.count > K_MAX_BODY_CHARACTER) {
            lString = lString.substring(to: K_MAX_BODY_CHARACTER)
        }
        return lString
    }
}
