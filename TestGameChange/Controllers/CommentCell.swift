
import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    
    var mCommentInfo : Comment? {
           didSet {
               configureCommentCellUI()
           }
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessoryType = .none
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCommentCellUI() {
        commentLbl.text = mCommentInfo?.body ?? ""
        userNameLbl.text = "User Name: " + (mCommentInfo?.user_name ?? "")
    }
}
