
import Foundation
struct IssesResult : Codable {
	let url : String?
	let repository_url : String?
	let labels_url : String?
	let comments_url : String?
	let events_url : String?
	let html_url : String?
	let id : Int?
	let node_id : String?
	let number : Int?
	let title : String?
	let user : User?
	let labels : [Labels]?
	let state : String?
	let locked : Bool?
	//let assignee : String?
	//let assignees : [String]?
	let milestone : String?
	let comments : Int?
	let created_at : String?
	let updated_at : String?
	let closed_at : String?
	let author_association : String?
	let active_lock_reason : String?
	let body : String?
	let performed_via_github_app : String?

	enum CodingKeys: String, CodingKey {

		case url = "url"
		case repository_url = "repository_url"
		case labels_url = "labels_url"
		case comments_url = "comments_url"
		case events_url = "events_url"
		case html_url = "html_url"
		case id = "id"
		case node_id = "node_id"
		case number = "number"
		case title = "title"
		case user = "user"
		case labels = "labels"
		case state = "state"
		case locked = "locked"
		//case assignee = "assignee"
		//case assignees = "assignees"
		case milestone = "milestone"
		case comments = "comments"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case closed_at = "closed_at"
		case author_association = "author_association"
		case active_lock_reason = "active_lock_reason"
		case body = "body"
		case performed_via_github_app = "performed_via_github_app"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		repository_url = try values.decodeIfPresent(String.self, forKey: .repository_url)
		labels_url = try values.decodeIfPresent(String.self, forKey: .labels_url)
		comments_url = try values.decodeIfPresent(String.self, forKey: .comments_url)
		events_url = try values.decodeIfPresent(String.self, forKey: .events_url)
		html_url = try values.decodeIfPresent(String.self, forKey: .html_url)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		node_id = try values.decodeIfPresent(String.self, forKey: .node_id)
		number = try values.decodeIfPresent(Int.self, forKey: .number)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		labels = try values.decodeIfPresent([Labels].self, forKey: .labels)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		locked = try values.decodeIfPresent(Bool.self, forKey: .locked)
		//assignee = try values.decodeIfPresent(String.self, forKey: .assignee)
		//assignees = try values.decodeIfPresent([String].self, forKey: .assignees)
		milestone = try values.decodeIfPresent(String.self, forKey: .milestone)
		comments = try values.decodeIfPresent(Int.self, forKey: .comments)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		closed_at = try values.decodeIfPresent(String.self, forKey: .closed_at)
		author_association = try values.decodeIfPresent(String.self, forKey: .author_association)
		active_lock_reason = try values.decodeIfPresent(String.self, forKey: .active_lock_reason)
		body = try values.decodeIfPresent(String.self, forKey: .body)
		performed_via_github_app = try values.decodeIfPresent(String.self, forKey: .performed_via_github_app)
	}

}


extension IssesResult{
static  let isoFormatter : ISO8601DateFormatter = {
    let formatter =  ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,]
    return formatter
   }()

    var dateFromString : Date  {
        let  iSO8601DateString = updated_at?.components(separatedBy: ".").reversed().joined(separator: ".") ?? ""
        return  IssesResult.isoFormatter.date(from: iSO8601DateString)!
    }
}
