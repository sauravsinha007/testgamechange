
import Foundation
struct Labels : Codable {
	let id : Int?
	let node_id : String?
	let url : String?
	let name : String?
	let color : String?
	let `default` : Bool?
	let description : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case node_id = "node_id"
		case url = "url"
		case name = "name"
		case color = "color"
		case `default` = "default"
		case description = "description"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		node_id = try values.decodeIfPresent(String.self, forKey: .node_id)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		color = try values.decodeIfPresent(String.self, forKey: .color)
		`default` = try values.decodeIfPresent(Bool.self, forKey: .default)
		description = try values.decodeIfPresent(String.self, forKey: .description)
	}

}
