//
//  Comment+CoreDataProperties.swift
//  TestGameChange
//
//  Created by Saurav Sinha on 06/01/21.
//  Copyright © 2021 Saurav Sinha. All rights reserved.
//
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment")
    }

    @NSManaged public var issue_number: Int16
    @NSManaged public var body: String?
    @NSManaged public var user_name: String?

}
