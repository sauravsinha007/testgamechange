//
//  Issue+CoreDataProperties.swift
//  TestGameChange
//
//  Created by Saurav Sinha on 06/01/21.
//  Copyright © 2021 Saurav Sinha. All rights reserved.
//
//

import Foundation
import CoreData


extension Issue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Issue> {
        return NSFetchRequest<Issue>(entityName: "Issue")
    }

    @NSManaged public var title: String?
    @NSManaged public var body: String?
    @NSManaged public var state: String?
    @NSManaged public var updated_at: String?
    @NSManaged public var number: Int16

}
