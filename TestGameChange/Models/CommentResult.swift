

import Foundation
struct CommentResult : Codable {
	let url : String?
	let html_url : String?
	let issue_url : String?
	let id : Int?
	let node_id : String?
	let user : User?
	let created_at : String?
	let updated_at : String?
	let author_association : String?
	let body : String?
	let performed_via_github_app : String?

	enum CodingKeys: String, CodingKey {

		case url = "url"
		case html_url = "html_url"
		case issue_url = "issue_url"
		case id = "id"
		case node_id = "node_id"
		case user = "user"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case author_association = "author_association"
		case body = "body"
		case performed_via_github_app = "performed_via_github_app"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		html_url = try values.decodeIfPresent(String.self, forKey: .html_url)
		issue_url = try values.decodeIfPresent(String.self, forKey: .issue_url)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		node_id = try values.decodeIfPresent(String.self, forKey: .node_id)
		user = try values.decodeIfPresent(User.self, forKey: .user)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
		author_association = try values.decodeIfPresent(String.self, forKey: .author_association)
		body = try values.decodeIfPresent(String.self, forKey: .body)
		performed_via_github_app = try values.decodeIfPresent(String.self, forKey: .performed_via_github_app)
	}

}
