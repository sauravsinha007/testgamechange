
import Foundation
import UIKit

extension String
{
    
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString)
    }
    
    
    func removeWhiteSpaceFromString() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }

}


extension UIView {
    
    func setLayout(WithBorderWidth borderWidth:CGFloat, borderColor:UIColor?, radius aRadius:CGFloat, andBackgroundColor bgColor:UIColor?)
    {
        self.layer.borderWidth = borderWidth
        self.layer.cornerRadius = aRadius
        if(borderColor != nil) {
            self.layer.borderColor = borderColor?.cgColor
        }
        
        if(bgColor != nil) {
            self.layer.backgroundColor = bgColor?.cgColor
        }
    }
}




extension UIViewController {
    func showAlertView(withMessage message: String?, andTitle title:String?){

         let lAlertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let lOkAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
        }
        
        lAlertController.addAction(lOkAction)
        self.present(lAlertController, animated: true, completion: nil)
        
    }
}
