

import UIKit

class Utils: NSObject {
    class func showAlert(_ reference:UIViewController,
                         message: String?,
                         title:String? ,
                         otherButtons:[String:((UIAlertAction)-> ())]? = nil,
                         cancelTitle: String = "OK",
                         cancelAction: ((UIAlertAction)-> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }
        reference.present(alert, animated: true, completion: nil)
    }
    
    class func getAppDocumentDirectoryPath() -> String
    {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print("documentsPath = \(documentsPath)")
        return documentsPath
    }
    
    class func saveContentInUserDefault(_ content:AnyObject, Inkey:String)
    {
        let defaults = UserDefaults.standard
        defaults.set(content, forKey: Inkey)
    }
    
    class func getContentFromUserDefautHavingKey(_ key:String) -> AnyObject?
    {
        var lAnyObject: AnyObject?
        let defaults = UserDefaults.standard
        if defaults.object(forKey: key) != nil
        {
            lAnyObject = defaults.object(forKey: key)! as AnyObject?
        }
        else {
            lAnyObject = nil
        }
        return lAnyObject
    }
    
    class func getDateInStringHavingDate(_ pDate:Date, andFormatter:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = andFormatter
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let lDateString : String =  formatter.string(from: pDate)
        return lDateString
    }
    
}




