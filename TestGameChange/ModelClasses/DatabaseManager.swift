//
//  DatabaseManeger.swift
//  TestGameChange
//
//  Created by Saurav Sinha on 06/01/21.
//  Copyright © 2021 Saurav Sinha. All rights reserved.
//

import Foundation
import CoreData

class DatabaseManager {
    
    static func saveIssuesListInLocalStorage(_ issuesList : [IssesResult]?) {
        if let lIssuesArray = issuesList {
            lIssuesArray.forEach { issueInfo in
                saveIssueInDb(issue: issueInfo)
            }
        }
    }
    
    static func saveIssueInDb(issue : IssesResult) {
        let lIssue = Issue(context: PersistentStorage.shared.context)
        lIssue.title = issue.title
        lIssue.body = issue.body
        if let lNumber = issue.number {
            lIssue.number = Int16(lNumber)
        }
        lIssue.updated_at = issue.updated_at
        lIssue.state = issue.updated_at
        PersistentStorage.shared.saveContext()
    }
    
    static func fetchIssueListFromLocalStroage() -> [Issue]? {
        let result = PersistentStorage.shared.fetchManagedObject(managedObject: Issue.self)
        return result
    }
    
    static func saveAllCommentsInLocalStorage(_ commentList : [CommentResult]?, _ issueNo : Int) {
        if let lCommnetArray = commentList {
            lCommnetArray.forEach { commentInfo in
                saveCommentInDb(comment: commentInfo, issueNo)
            }
        }
    }
    
    static func saveCommentInDb(comment : CommentResult, _ issueNo : Int) {
        let lComment = Comment(context: PersistentStorage.shared.context)
        lComment.body = comment.body
        lComment.user_name = comment.user?.login ?? ""
        lComment.issue_number = Int16(issueNo)
        PersistentStorage.shared.saveContext()
    }
    
    static func fetchCommenFromDbHaving(issueNo : Int16) -> [Comment]? {
        let fetchRequest = NSFetchRequest<Comment>(entityName: "Comment")
        let predicate = NSPredicate(format: "issue_number == \(Int(issueNo))")
        fetchRequest.predicate = predicate
        do {
            let result = try PersistentStorage.shared.context.fetch(fetchRequest)
            //guard result != nil else {return nil}
            return result

        } catch let error {
            debugPrint(error)
        }

        return nil
    }
    
    static func deleteAllDataFromLocalStorage() {
        deleteDataFromEntity("Issue")
        deleteDataFromEntity("Comment")
    }
    
    static func deleteDataFromEntity(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try PersistentStorage.shared.context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                PersistentStorage.shared.context.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }

    
}
