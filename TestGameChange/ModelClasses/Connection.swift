
import UIKit

class Connection: NSObject {
    
    var requsetTimeOut:TimeInterval = 15;
    
    func startConnectionHaving(_ urlString:String ,methodType:String, postData:Data? = nil , onCompletion: @escaping ServiceResponse)
    {
        let url:URL = URL(string: urlString)!
        let config = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: url)
        request.httpMethod = methodType
        request.timeoutInterval = requsetTimeOut;
        if (methodType == "POST")
        {
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpBody = postData;
        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            if(data != nil) {
                onCompletion(data!, error as NSError?)
            }
            else
            {
                print("Error response >>>> \(String(describing: error)) \n error?.localizedDescription = \(String(describing: error?.localizedDescription))")
                onCompletion(nil, error as NSError?)
            }
            
        })
        
        task.resume()
    }
 
}
