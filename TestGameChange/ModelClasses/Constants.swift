
import UIKit

let KIssuesURL =  "https://api.github.com/repos/firebase/firebase-ios-sdk/issues"
let K_COMMENT_NOT_AVAILABLE_MSG =  "Comments not available"
let K_SAVE_LAST_STNC_TIME =  "save_last_sync_time"

typealias ServiceResponse = (Data?, NSError?) -> Void

