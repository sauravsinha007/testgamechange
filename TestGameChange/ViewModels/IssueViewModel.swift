
import Foundation
let k_OPEN_STATE = "open"

class IssueViewModel {
    var refreshViewClouser : ((_ error : String?) -> Void)?
    var refreshDetailViewClouser : ((_ error : String?) -> Void)?
    func getIssuesListFromServer()
    {
        let lConnection = Connection()
        lConnection.startConnectionHaving(KIssuesURL,
                                                    methodType: "GET",
                                                    onCompletion:{ (responseData,responseError) in
                                                        do {
                                                            if let lResponseData = responseData {
                                                                if let lParseOject = try JSONSerialization.jsonObject(with: responseData!, options:JSONSerialization.ReadingOptions.mutableContainers ) as? [String : String], let lMsg = lParseOject["message"], !lMsg.isEmpty  {
                                                                    self.refreshViewClouser?(lMsg)
                                                                    return
                                                                }
                                                                  
                                                                
                                                                let lResponseModal = try JSONDecoder().decode([IssesResult].self, from: lResponseData)
                                                                self.mapOpenIssuesList(lResponseModal)
                                                                self.saveLastSyncDataTime()
                                                            }
                                                        } catch {
                                                            print("Exception error = \(error)")
                                                        }
                                                        self.refreshViewClouser?(responseError?.localizedDescription)
        })
        
    }
    
    func mapOpenIssuesList(_ issuesArray : [IssesResult]?) {
        if let lAllIssesArray = issuesArray, !lAllIssesArray.isEmpty {
            let lOpenIssues = lAllIssesArray.filter({$0.state == k_OPEN_STATE})
            let lLatestssues = lOpenIssues.sorted{ $0.dateFromString > $1.dateFromString  }
            DatabaseManager.saveIssuesListInLocalStorage(lLatestssues)
        }
    }
    
    func saveLastSyncDataTime() {
         let lCurrentDate = Utils.getDateInStringHavingDate(NSDate() as Date, andFormatter: "YYYY-MM-dd HH:mm:ss")
         Utils.saveContentInUserDefault(lCurrentDate as AnyObject, Inkey: K_SAVE_LAST_STNC_TIME)
    }
    
    func checkAndRemoveLocalData() {
        if let lSaveLastSyncTime = Utils.getContentFromUserDefautHavingKey(K_SAVE_LAST_STNC_TIME) as? String, !lSaveLastSyncTime.isEmpty {
            let lCurrentDateStr = Utils.getDateInStringHavingDate(NSDate() as Date, andFormatter: "YYYY-MM-dd HH:mm:ss")
            
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss";
            if let lNowDate = dateFormatter.date(from: lCurrentDateStr), let lLastDate = dateFormatter.date(from: lSaveLastSyncTime) {
                let diff = Int(lNowDate.timeIntervalSince1970 - lLastDate.timeIntervalSince1970)
                let hours = diff / 3600
                //print("hours = \(hours) --- lNowDate = \(lNowDate) --- lLastDate = \(lLastDate)")
                if hours >= 24 {
                    DatabaseManager.deleteAllDataFromLocalStorage()
                }
            }
        }
        
    }
    
    func getCommentsOnIssuesFromServerHaving(issueNo : Int)
    {
        //commentList.removeAll()
        let lUrl = KIssuesURL + "/" + "\(issueNo)" + "/comments"
        let lConnection = Connection()
        lConnection.startConnectionHaving(lUrl,
                                                    methodType: "GET",
                                                    onCompletion:{ (responseData,responseError) in
                                                        do {
                                                            if let lParseOject = try JSONSerialization.jsonObject(with: responseData!, options:JSONSerialization.ReadingOptions.mutableContainers ) as? [String : String], let lMsg = lParseOject["message"], !lMsg.isEmpty  {
                                                                self.refreshViewClouser?(lMsg)
                                                                return
                                                            }
                                                            
                                                            if let lResponseData = responseData {
                                                                let lCommetModal = try JSONDecoder().decode([CommentResult].self, from: lResponseData)
                                                                //self.commentList.append(contentsOf: lCommetModal)
                                                                self.saveComments(lCommetModal, issueNo: issueNo)
                                                            }
                                                        } catch {
                                                            print("Exception error = \(error)")
                                                        }
                                                        self.refreshDetailViewClouser?(responseError?.localizedDescription)
        })
        
    }

    func saveComments(_ comments : [CommentResult]?, issueNo: Int) {
        DatabaseManager.saveAllCommentsInLocalStorage(comments, issueNo)
    }
}

